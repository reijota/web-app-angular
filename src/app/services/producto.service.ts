import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

import { Producto } from '../models/producto';
import { GLOBAL } from './global';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  public url: string;
  public errorHandl;

  constructor(private _http: HttpClient) {
    this.url = GLOBAL.url;
   }

  getProductos() {
    console.log(this.url + 'productos');
    return this._http.get(this.url + 'productos')
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  getProducto(id) {
    return this._http.get(this.url + 'producto/' + id)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
    );
  }

  addProducto(producto: Producto){
    let json = JSON.stringify(producto);
    let params = 'json=' + json;
    let headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});

    return this._http.post(this.url + 'productos', params, {headers: headers})
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  editProducto(id, producto: Producto) {
    let json = JSON.stringify(producto);
    let params = "json=" + json;
    let headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});

    console.log(json);

    return this._http.post(this.url + 'update-producto/' + id, params, {headers: headers})
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  deleteProducto(id) {
    return this._http.get(this.url + 'delete-producto/' + id)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }

  makeFileRequest(url: string, params: Array<string>, files: Array<File>) {
    return new Promise((resolve, reject) => {
      // creamos la variables para tener un formulario, y hacer peticiones ajax
      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();

      // recorremos el array files, y lo vamos añadiendo a formData
      for(var i = 0; i < files.length; i++){
        formData.append('uploads[]', files[i], files[i].name);
      }

      // cuando la peticion ajax este preparada resolvemos la subida
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          }else{
            reject(xhr.response);
          }
        }
      };

      // havemos la peticion y la lanzamos
      xhr.open('POST', url, true);
      xhr.send(formData);
    });
  }
}
